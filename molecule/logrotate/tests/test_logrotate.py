import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conserver_logrotate(host):
    # This function is intended to test the following:
    # - conserver rotates a logfile that is > 50 MB
    # - logrotate picks up the rotated logfile, compresses, and moves it to a separate directory
    # - logrotate no longer picks up rotated (and compressed) logfile

    logdir = "/var/log/conserver"

    assert host.file(logdir).is_directory

    assert host.file(f"{logdir}/console-test-console.log").is_file

    assert host.file(f"{logdir}/console-test-console.log").size > 1024 * 1024 * 50

    # Trigger conserver logrotation:
    #  - needs an active console session that is up
    with host.sudo():
        host.run("/tmp/trigger_conserver_logrotate.py")

    assert host.file(f"{logdir}/console-test-console.log").size < 1024 * 1024

    # Get the name of the rotated file
    rotated = None
    for log in host.file(logdir).listdir():
        if host.file(f"{logdir}/{log}").size > 1024 * 1024 * 50:
            rotated = log

    assert rotated

    # Run logrotate to compress the log
    with host.sudo():
        host.run("logrotate /etc/logrotate.d/conserver")

    assert host.file(f"{logdir}/old-console-logs").is_directory

    assert host.file(f"{logdir}/old-console-logs/{rotated}.1.gz").is_file

    # Run logrotate again
    with host.sudo():
        host.run("logrotate /etc/logrotate.d/conserver")

    # Check that it did not compress the log again
    # previous version of role had a bug where logrotate kept compressing already compressed logs creating `foo.1.gz.gz.gz.gz` files
    assert host.file(f"{logdir}/old-console-logs/{rotated}.1.gz").is_file
