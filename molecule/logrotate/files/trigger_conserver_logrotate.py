#!/usr/bin/env python3

"""
This script is written to be compatible with Python-3.5 (e.g. no f-strings)
Reason: the Yocto distribution used at ESS has Python-3.5
"""

import os
import shlex
import socket
import subprocess
import time


def wait_for_logrotation(logfile):
    # Conserver checks the size of the log file every ~ 5 mins (5 mins plus a random wait between 0-60 seconds)
    # so we wait for at most 6 minutes
    for i in range(1, 6 * 60):
        # Break as soon as the log file is rotated; i.e. its size is less than 50 MB
        if os.stat(logfile).st_size < 50 * 1024 * 1024:  # 50 MB
            break
        time.sleep(1)


class UDS(object):
    def __init__(self, path):
        self.udspath = path
        self.control = socket.socket(socket.AF_UNIX)

    def __enter__(self):
        self.control.bind(self.udspath)
        self.control.listen()
        return self.control

    def __exit__(self, type, value, traceback):
        os.remove(self.udspath)
        self.control.close()


class Console(object):
    def __init__(self, name):
        self.cmd = "console -s {console_name}".format(console_name=name)
        self.proc = None

    def __enter__(self):
        self.proc = subprocess.Popen(shlex.split(self.cmd))
        return self.proc

    def __exit__(self, type, value, traceback):
        self.proc.terminate()


def run():
    console_name = "test-console"
    logfile = "/var/log/conserver/console-{console_name}.log".format(
        console_name=console_name
    )
    uds_dir = "/run/ioc@{console_name}".format(console_name=console_name)

    try:
        os.mkdir(uds_dir)
    except FileExistsError:
        pass

    # Fake an IOC
    with UDS("{uds_dir}/control".format(uds_dir=uds_dir)) as control:
        # Conserver only checks log files for consoles that are UP so we start one
        with Console(console_name):
            (cl, cl_info) = control.accept()

            wait_for_logrotation(logfile)

            cl.close()


if __name__ == "__main__":
    run()
