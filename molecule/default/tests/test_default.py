import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

CONSOLES_CF = "/etc/conserver/consoles.cf"


def test_conserver_paths(host):
    assert host.file("/var/log/conserver").is_directory
    assert host.file("/etc/conserver").is_directory
    assert host.file(CONSOLES_CF).is_file

    if "ubuntu" in host.ansible.get_variables()["inventory_hostname"]:
        conserver_config_path = "/etc/conserver"
    else:
        conserver_config_path = "/etc"

    assert host.file(os.path.join(conserver_config_path, "conserver.cf")).is_file
    assert host.file(os.path.join(conserver_config_path, "conserver.passwd")).is_file


def test_conserver_procs_cf(host):
    if "ansible_consoles" in host.ansible.get_variables()["group_names"]:
        assert host.file(CONSOLES_CF).contains("console test-console")
        assert host.file(CONSOLES_CF).contains("uds /tmp/test-console/control")
        assert host.file(CONSOLES_CF).contains("master master_server")
        assert host.file(CONSOLES_CF).contains("type uds")
    else:
        assert not host.file(CONSOLES_CF).contains("console")


def test_conserver_server_enabled_and_running(host):
    service_name = "conserver"
    if "ubuntu" in host.ansible.get_variables()["inventory_hostname"]:
        service_name = "conserver-server"

    service = host.service(service_name)
    if "ansible_consoles" in host.ansible.get_variables()["group_names"]:
        assert service.is_enabled
        assert service.is_running
    else:
        assert not service.is_enabled
        assert not service.is_running


def test_conserver_was_reloaded(host):
    old_pidfile = "/tmp/old-conserver.pid"
    current_pidfile = "/var/run/conserver.pid"
    if "ubuntu" in host.ansible.get_variables()["inventory_hostname"]:
        current_pidfile = "/run/conserver/conserver.pid"

    # Check that conserver was reloaded instead of restarted
    if "ansible_consoles" in host.ansible.get_variables()["group_names"]:
        assert host.file(old_pidfile).is_file
        assert host.file(current_pidfile).is_file
        assert host.file(old_pidfile).content_string == host.file(current_pidfile).content_string


def test_conserver_cf(host):
    conserver_cf = "/etc/conserver.cf"
    if "ubuntu" in host.ansible.get_variables()["inventory_hostname"]:
        conserver_cf = "/etc/conserver/conserver.cf"

    if "ansible_consoles" in host.ansible.get_variables()["group_names"]:
        assert host.file(conserver_cf).contains("trusted trusted-host-1;")
        assert host.file(conserver_cf).contains("trusted trusted-host-2;")
        assert host.file(conserver_cf).contains("allowed allowed-host;")
        assert host.file(conserver_cf).contains("rejected rejected-host;")


def test_conserver_finds_our_local_consoles(host):
    if "ansible_consoles" in host.ansible.get_variables()["group_names"]:
        assert host.exists("console")
        assert host.run_test("console -I")
        # -I is the same as -i but acts only on the primary server
        console_output = host.run("console -I")
        """
          The -i option outputs status information regarding each console in 15 colon-separated fields.
          https://linux.die.net/man/1/console
        """
        consoles = console_output.stdout.splitlines()

        # Make sure there is only 1 local console
        assert len(consoles) == 1

        (
            console_name,
            console_hostname_pid_socket,
            console_type,
            console_details,
            console_users_list,
            console_state,
            console_perm,
            console_logfile_details,
            console_break_seq,
            console_reup,
            console_aliases,
            console_options,
            console_initcmd,
            console_idletimeout,
            console_idlestring,
        ) = consoles[0].split(":")

        log_info = console_logfile_details.split(",")
        assert console_name == "ioc-test_console"

        # Check that it is UDS; `%' represents a Unix domain socket
        assert console_type == "%"

        # Check the UDS location
        assert console_details.startswith("/run/ioc@ioc-test_console/control,")

        logfile = "/var/log/conserver/console-ioc-test_console.log"
        assert logfile in log_info

        # These options are set via the `ioc` group so what we really test here is that it got included

        # Check that activity logging is set
        assert "act" in log_info

        # Check that timestamp interval is set to 1 day
        assert f"{24 * 60 * 60}" in log_info

        # Check for noautoup; i.e. no automatic reconnection of downed consoles
        assert "noautoup" == console_reup

        # Check that _only_ ondemand and login are set
        assert set(["ondemand", "login"]) == set(console_options.split(","))
