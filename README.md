# ics-ans-role-conserver-server

Ansible role to install [conserver server](https://www.conserver.com).
Currently assumes that conserver client connects via UDS.
Changes will be required to support telnet ports.

It is possible to copy console configurations to `procs_cf_path` and then notify `reload conserver`

## Role Variables


```yaml
conserver_server_host: Conserver server hostname
conserver_server_consoles:
  - master: IOC host - must be 'localhost' if type = 'uds' [default: localhost]
    name: IOC name for conserver usage (required)
    type: console type (typically 'uds')
    uds: path to UNIX domain socket
```

## Example Playbooks

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conserver-server
```

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conserver-server
  tasks:
    - name: Copy custom console config
      copy:
        src: custom_console
        dest: "{{ procs_cf_path }}/custom-console.cf"
        owner: root
        group: root
        mode: 0644
      notify: reload conserver
```

## License

BSD 2-clause
